[![Java CI with Maven](https://github.com/jamilxt/java_spring-boot_japser-report/actions/workflows/maven.yml/badge.svg)](https://github.com/jamilxt/java_spring-boot_japser-report/actions/workflows/maven.yml)
# Jasper Report using Spring Boot (Jave 11, Maven)
## Export (PDF, EXCEL, CSV, DOCX)
### Reference Documentation
Run the application.
Then, Export:
* PDF: [http://localhost:8080/reports/transactions/download?exportType=PDF](http://localhost:8080/reports/transactions/download?exportType=PDF) <br><br>
 <br><br>
* EXCEL: [http://localhost:8080/reports/transactions/download?exportType=EXCEL](http://localhost:8080/reports/transactions/download?exportType=EXCEL) <br><br>
 <br><br>
* CSV: [http://localhost:8080/reports/transactions/download?exportType=CSV](http://localhost:8080/reports/transactions/download?exportType=CSV) <br><br>
 <br><br>
* DOCX: [http://localhost:8080/reports/transactions/download?exportType=DOCX](http://localhost:8080/reports/transactions/download?exportType=DOCX) <br><br>
<br><br>

### Download [Jaspersoft Studio](https://community.jaspersoft.com/project/jaspersoft-studio) to modify the template (file with .jrxml extension) as you like.
#### Templates are: 
* PDF: transaction_report_pdf.jrxml
* EXCEL: transaction_report_excel.jrxml
* CSV: transaction_report_csv.jrxml
* DOCX: transaction_report_docx.jrxml

Open these templates using Jasper Studio & modify as your own. It takes sometime to be familiar with the syntax to design the template. It's similar to HTML but need some practice to make a better design. 
Enjoy!
